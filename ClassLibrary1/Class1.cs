﻿using System;
using SkiaSharp;

namespace ClassLibrary1
{
    public class Class1
    {
        public SKBitmap bitmap = null;

#if OSX
        public OsxOnlyLibrary.Class1 osxRef = null;
#endif

        public NetStandardLibrary.Class1 netRef = null;
    }
}
