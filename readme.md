# Multi-Target Test

This is an example project to demonstrate problems with configuing a .NET project that targets `net461` and `netcoreapp2.0` on Windows and only `netcoreapp2.0` on OSX.


## Background and Goal

The real project behind this is a GUI library ("GuiKit") that provides a cross platform app framework for Windows and OSX.  It internally uses PInvoke to call Win32 API on Windows and the Objective-C runtime on OSX.  `netstandard2.0` isn't an option for most of this as the Objective-C interop uses MethodBuilder and related classes that aren't available in netstandard.

Some goals/requirements:

* The project also depends on one NuGet package (SkiaSharp), so it's important any solution for this also works with the NuGet client.

* The project needs multiple configurations:  `DebugWin`, `ReleaseWin`, `DebugOsx` and `ReleaseOsx`

* Ideally the `Win` configurations would target `net461` (and maybe `netcoreapp2.0`) while the `Osx` configurations would target `netcoreapp2.0` only.

* There is also one "primitives" project that doesn't require anything outside `netstandard` - I've included one such project in this example because it highlights another problem with the `dotnet build` system.

This repo contains branches for each of the approaches that I've tried.


## Example Solution

The example solution includes three projects:

* `ClassLibrary1` - the main library we're trying to build
* `OsxOnlyLibrary` - a library only used by ClassLibrary1 on the OSX build
* `NetStandardLibrary` - a `netstandard2.0` library used by ClassLibrary1

`ClassLibrary1` also includes a reference to the NuGet package `SkiaSharp`

## Approach 1

Here's the first approach to this:

~~~
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <Configurations>DebugWin;ReleaseWin;DebugOsx;ReleaseOsx</Configurations>
    <TargetFrameworks>netcoreapp2.0;net461</TargetFrameworks>
  </PropertyGroup>

  <PropertyGroup Condition="$(Configuration.EndsWith('Win'))">
    <DefineConstants>$(DefineConstants);WINDOWS</DefineConstants>
  </PropertyGroup>
  
  <PropertyGroup Condition="$(Configuration.EndsWith('Osx'))">
    <DefineConstants>$(DefineConstants);OSX</DefineConstants>
  </PropertyGroup>

  <ItemGroup>
    <PackageReference Include="SkiaSharp" Version="1.60.3" />
  </ItemGroup>

  <ItemGroup>
    <ProjectReference Include="..\NetStandardLibrary\NetStandardLibrary.csproj" />
  </ItemGroup>

  <ItemGroup Condition="$(Configuration.EndsWith('Osx'))">
    <ProjectReference Include="..\OsxOnlyLibrary\OsxOnlyLibrary.csproj" />
  </ItemGroup>

</Project>
~~~


This works perfectly when building on Windows - both in VS and via `dotnet build`.  But...

1. It requires adding a net461 target framework to the OsxOnlyLibrary (even though it's conditionally referenced)
2. It fails on OS X:

~~~
dotnet build -c DebugOsx
~~~

~~~
/usr/local/share/dotnet/sdk/2.1.402/Microsoft.Common.CurrentVersion.targets(1179,5): error MSB3644: The reference assemblies for framework ".NETFramework,Version=v4.6.1" were not found. To resolve this, install the SDK or Targeting Pack for this framework version or retarget your application to a version of the framework for which you have the SDK or Targeting Pack installed. Note that assemblies will be resolved from the Global Assembly Cache (GAC) and will be used in place of reference assemblies. Therefore your assembly may not be correctly targeted for the framework you intend. [/Users/brad/Projects/MultiTargetTest/ClassLibrary1/ClassLibrary1.csproj]
~~~

Obviously net461 framework isn't supported on OSX.  So try building for just netcoreapp2.0:

~~~
dotnet build -c DebugOsx -f netcoreapp2.0
~~~

~~~
/usr/local/share/dotnet/sdk/2.1.402/Sdks/Microsoft.NET.Sdk/targets/Microsoft.PackageDependencyResolution.targets(198,5): error NETSDK1005: Assets file '/Users/brad/Projects/MultiTargetTest/NetStandardLibrary/obj/project.assets.json' doesn't have a target for '.NETCoreApp,Version=v2.0'. Ensure that restore has run and that you have included 'netcoreapp2.0' in the TargetFrameworks for your project. [/Users/brad/Projects/MultiTargetTest/NetStandardLibrary/NetStandardLibrary.csproj]
~~~

Doesn't work because the required NetStandardLibrary project targets netstandard, not netcoreapp2.

## Approach 2

Restrict the OSX build to just `netcoreapp2.0` by updating both ClassLibrary1 and OsxOnlyLibrary as follows:

~~~
<PropertyGroup>
  <Configurations>DebugWin;ReleaseWin;DebugOsx;ReleaseOsx</Configurations>
  <TargetFrameworks>netcoreapp2.0;net461</TargetFrameworks>
  <TargetFrameworks Condition="$(Configuration.EndsWith('Osx'))">netcoreapp2.0</TargetFrameworks>
</PropertyGroup>
~~~

(Also update solution file to map configurations correctly).

Now builds on osx:

~~~
dotnet build -c DebugOsx
dotnet build -c ReleaseOsx
~~~

And the Win configurations fail (as expected and acceptable).

## Approach 3

Back to Windows...  dotnet build works fine for all configurations:

~~~
dotnet build -c DebugWin
dotnet build -c ReleaseWin
dotnet build -c DebugOsx
dotnet build -c ReleaseOsx
~~~


VS2017 works for the OSX configurations, but not for the Win configs:

~~~
1>------ Rebuild All started: Project: NetStandardLibrary, Configuration: Debug Any CPU ------
2>------ Rebuild All started: Project: OsxOnlyLibrary, Configuration: DebugWin Any CPU ------
2>C:\Program Files\dotnet\sdk\2.1.402\Sdks\Microsoft.NET.Sdk\targets\Microsoft.PackageDependencyResolution.targets(198,5): error NETSDK1005: Assets file 'C:\Users\Brad\Projects\MultiTargetTest\OsxOnlyLibrary\obj\project.assets.json' doesn't have a target for '.NETFramework,Version=v4.6.1'. Ensure that restore has run and that you have included 'net461' in the TargetFrameworks for your project.
2>Done building project "OsxOnlyLibrary.csproj" -- FAILED.
1>NetStandardLibrary -> C:\Users\Brad\Projects\MultiTargetTest\NetStandardLibrary\bin\Debug\netstandard2.0\NetStandardLibrary.dll
3>------ Rebuild All started: Project: ClassLibrary1, Configuration: DebugWin Any CPU ------
3>C:\Program Files\dotnet\sdk\2.1.402\Sdks\Microsoft.NET.Sdk\targets\Microsoft.PackageDependencyResolution.targets(198,5): error NETSDK1005: Assets file 'C:\Users\Brad\Projects\MultiTargetTest\ClassLibrary1\obj\project.assets.json' doesn't have a target for '.NETFramework,Version=v4.6.1'. Ensure that restore has run and that you have included 'net461' in the TargetFrameworks for your project.
3>Done building project "ClassLibrary1.csproj" -- FAILED.
========== Rebuild All: 1 succeeded, 2 failed, 0 skipped ==========
~~~


Let's try to be more specific with the conditions:

~~~
  <PropertyGroup>
    <Configurations>DebugWin;ReleaseWin;DebugOsx;ReleaseOsx</Configurations>
    <TargetFrameworks Condition="$(Configuration.EndsWith('Win'))">netcoreapp2.0;net461</TargetFrameworks>
    <TargetFrameworks Condition="$(Configuration.EndsWith('Osx'))">netcoreapp2.0</TargetFrameworks>
  </PropertyGroup>
~~~

Same error.


## Approach 4

Let's try switching to `TargetFramework` (without the 's') and removing netcoreapp2.0 for the win build.

~~~
  <PropertyGroup>
    <Configurations>DebugWin;ReleaseWin;DebugOsx;ReleaseOsx</Configurations>
    <TargetFramework Condition="$(Configuration.EndsWith('Win'))">net461</TargetFramework>
    <TargetFramework Condition="$(Configuration.EndsWith('Osx'))">netcoreapp2.0</TargetFramework>
  </PropertyGroup>
~~~

This now works in:

* Visual Studio 2017
* dotnet build on Windows and OSX
* JetBrains Rider on OSX

But:

* we've lost the Windows .netcoreapp build and
* fails in Visual Studio for Mac complaining about invalid configuration.


## Conclusion

I seem to have finally found something that mostly works, however:

* It shouldn't be this hard
* Sometimes things will work if you rebuild in VS enough.  eg: I've had some configurations where on the third rebuild it works.  
* Sometimes it will work (or appear to be working) but after closing and re-opening Visual Studio it's broken again.
* Still don't have a way to target netcoreapp and net461 on the Windows build - why does TargetFramework work, but TargetFrameworks not?
* Why doesn't `dotnet build -f netcoreapp2.0` build netstandard libraries?
* On Windows I still need to build a net461 version of OsxOnlyLibrary - even though it's never used.

Also in this example I couldn't reproduce the problems I was getting with the real GuiKit project relating to NuGet.  In particular it seemed that the target framework wasn't resolved correctly and was getting errors about target framework ''.  I'll update this project if I can figure out how to provoke that. (I updated to VS 15.8.5 - perhaps that's improved things slightly).


## NuGet Packaging

According to [this post](https://natemcmaster.com/blog/2016/05/19/nuget3-rid-graph/) it should be possible to target assemblies for
different runtimes by packaging the above in the nuget package like so (where assemblies are placed in `runtimes\{rid}\lib\{tfm}`:

~~~
    <file src=".\ClassLibrary1\bin\ReleaseWin\net461\ClassLibrary1.*" target="runtimes\win\lib\net461" />
    <file src=".\ClassLibrary1\bin\ReleaseWin\net461\NetStandardLibrary.*" target="runtimes\win\lib\net461" />

    <file src=".\ClassLibrary1\bin\ReleaseOsx\netcoreapp2.0\ClassLibrary1.*" target="runtimes\osx\lib\netcoreapp2.0" />
    <file src=".\ClassLibrary1\bin\ReleaseOsx\netcoreapp2.0\NetStandardLibrary.*" target="runtimes\osx\lib\netcoreapp2.0" />
    <file src=".\ClassLibrary1\bin\ReleaseOsx\netcoreapp2.0\OsxOnlyLibrary.*" target="runtimes\osx\lib\netcoreapp2.0" />
~~~

Although creation of the nupkg works without errors or warnings, when the resulting package is added to another project, the references to the assemblies aren't added in the target project.

I've seen mention of a `ref` subfolder of the NuGet package but haven't been able to find any documentation describing this.

The `NuGet` branch of this repository includes a demo `.nuspec` file and a `build.bat` file that builds the project and the nuget package.

Associated StackOverflow questions: https://stackoverflow.com/questions/52397501/nuget-references-to-assemblies-in-runtimes-folder-not-added

